import axios from "axios";
import { useNavigate } from "react-router"

export default function RegisterPage() {
  const navigateTo = useNavigate();

  async function register() {
    const form = new FormData()
    form.append('name', document.getElementById('name').value);
    form.append('email', document.getElementById('email').value);
    form.append('password', document.getElementById('password').value);
    form.append('password_confirmation', document.getElementById('password').value);

    await axios.get('/sanctum/csrf-cookie');
    const response = await axios.post('/register', form);

    if (response.status === 201) {
      navigateTo('/dashboard');
    }
  }

  return (
    <div className="w-screen h-screen flex flex-row">
      <div className="h-screen w-[60%] bg-white flex flex-col items-center justify-center" >
        <div className="flex flex-col items-center p-20 border-2 border-slate-200">
          <h1 className="font-poppins font-bold text-5xl text-black">Create Account</h1>
          <div className="h-3" />
          <p className="text-lg font-poppins text-black">Already have an account? {' '}
            <span className="text-blue-400 underline font-poppins text-lg cursor-pointer" onClick={() => navigateTo('/login')}>Sign in</span>
          </p>
          <div className="h-14" />

          <form className="flex flex-col w-full">
            <label htmlFor="name">
              <input className="p-3 w-full bg-slate-100" id="name" name="name" type="text" placeholder="Your name" />
            </label>
            <div className="h-6" />
            <label htmlFor="email">
              <input className="p-3 w-full bg-slate-100" id="email" name="email" type="email" placeholder="name@provider.com" />
            </label>
            <div className="h-6" />
            <label htmlFor="password">
              <input className="p-3 w-full bg-slate-100" id="password" name="password" type="password" placeholder="••••••••" />
            </label>

            <div className="h-6" />
            <button className="w-full bg-blue-600 text-white font-poppins text-lg rounded-xl px-2 py-2" type="button" onClick={register}>
              Sign up
            </button>
          </form>

        </div>
      </div>
      <div className="h-screen w-[40%] bg-red-200 bg-center bg-cover bg-no-repeat"
        style={{ backgroundImage: `url(https://images.pexels.com/photos/2322425/pexels-photo-2322425.png)` }} />
    </div>
  )
}