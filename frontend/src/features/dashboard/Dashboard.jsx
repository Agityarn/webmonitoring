import ChartSquareBar from "../../icons/ChartSquareBar"
import Table from "../../icons/Table"
import Report from "../../icons/Report"
import { useNavigate } from 'react-router';
import User from "../../icons/User";
import ChartPage, { ActivePowerChart, CurrentChart, FrequencyChart, PowerFactorChart, VoltageChart } from "../chart/ChartPage";
import { useEffect, useMemo, useState } from "react";
import TablePage from "../table/TablePage";
import axios from 'axios';

function ReportElement() {
  return (
    <div className="flex items-center justify-center w-full h-full">
      <h1 className="text-black text-7xl">Report</h1>
    </div>
  )
}

function Dashboard() {
  const navigateTO = useNavigate();

  // dataset
  const [dataset, sDataset] = useState([{ label: '*', current: 0, voltage: 0, powerFactor: 0, activePower: 0, frequency: 0 }]);
  // useEffect(() => {
  //     const newArr = [...dataset];
  //     if (newArr.length === 50) newArr.shift();

  //     const date = new Date();
  //     const time = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
  //     const current = Math.floor(Math.random() * 400);
  //     const voltage = Math.floor(Math.random() * 500);
  //     const powerFactor = Math.floor(Math.random() * 2) - 1;
  //     const activePower = Math.floor(Math.random() * 250);
  //     const frequency = Math.floor(Math.random() * 50);

  //     newArr.push({
  //       label: time,
  //       current: current,
  //       voltage: voltage,
  //       powerFactor: powerFactor,
  //       activePower: activePower,
  //       frequency: frequency
  //     });
  //     sDataset(newArr);
  //     console.log(dataset)
  // } ,[dataset]);

  useEffect(() => {
    axios
    .put('http://192.168.1.205/sys/log_in',{"password": "00000000"},{withCredentials:false})
    .then((value) => {
      axios
      .get('http://192.168.1.205/data/tags/M221:Current',{headers:{Cookie:value.data.session_id},withCredentials:false})
      .then((value) => {console.log(value.data)
    });
  }, [])});

  const [showSubMenu, setShowSubMenu] = useState(false);
  const [localRoute, setLocalRoute] = useState('CHART');

  /* Available menu:
   * ALL, CURRENT, VOLTAGE, POWERFACTOR, ACTIVEPOWER, FREQUENCY
   */
  const [currentMenu, setCurrentMenu] = useState('ALL');

  const routeElement = useMemo(() => {
    if (localRoute === 'CHART' && currentMenu === 'ALL') {
      return <ChartPage dataset={dataset} />
    } else if (localRoute === 'CHART' && currentMenu === 'CURRENT') {
      return (
        <div className="w-full h-full flex flex-col">
          <div className="w-full h-1/2"><CurrentChart data={dataset} /></div>
          <div className="w-full h-1/2 bg-green-400" />
        </div>
      );
    } else if (localRoute === 'CHART' && currentMenu === 'VOLTAGE') {
      return (
        <div className="w-full h-full flex flex-col">
          <div className="w-full h-1/2"><VoltageChart data={dataset} /></div>
          <div className="w-full h-1/2 bg-green-400" />
        </div>
      );
    } else if (localRoute === 'CHART' && currentMenu === 'POWERFACTOR') {
      return (
        <div className="w-full h-full flex flex-col">
          <div className="w-full h-1/2"><PowerFactorChart data={dataset} /></div>
          <div className="w-full h-1/2 bg-green-400" />
        </div>
      );
    } else if (localRoute === 'CHART' && currentMenu === 'ACTIVEPOWER') {
      return (
        <div className="w-full h-full flex flex-col">
          <div className="w-full h-1/2"><ActivePowerChart data={dataset} /></div>
          <div className="w-full h-1/2 bg-green-400" />
        </div>
      );
    } else if (localRoute === 'CHART' && currentMenu === 'FREQUENCY') {
      return (
        <div className="w-full h-full flex flex-col">
          <div className="w-full h-1/2"><FrequencyChart data={dataset} /></div>
          <div className="w-full h-1/2 bg-green-400" />
        </div>
      );
    }

    else if (localRoute === 'TABLE') {
      return <TablePage dataset={dataset} />
    } else {
      return <ReportElement />
    }
  }, [localRoute, currentMenu, dataset]);

  const onChartClick = () => {
    setShowSubMenu(!showSubMenu);
    setCurrentMenu('ALL');
  }

  return (
    <div className="w-screen h-screen flex flex-col">
      <div className=' flex flex-row justify-between w-full h-12 bg-sky-900' >
        <p className="text-white font-anton text-3xl pl-3 pt-2">Dashboard</p>
        <p className="pt-1 pr-2 hover:bg-sky-700"><User /></p>
      </div>
      <div className="h-full flex flex-row">
        <div className="bg-sky-900 w-48 flex flex-col w- h-full pt-3 gap-2">
          <div className="flex flex-col space-x-2 pl-1" >
            <div className="flex flex-row hover:bg-sky-700 " onClick={onChartClick}>
              <ChartSquareBar />
              <span className="text-white text-xl cursor-pointer" onClick={() => setLocalRoute('CHART')} > Chart</span>
            </div>
            <div className={`flex-col flex gap-2 ${showSubMenu ? 'block' : 'hidden'}`}>
              <button className="text-left pl-2 text-white" onClick={() => setCurrentMenu('CURRENT')}>Current</button>
              <button className="text-left pl-2 text-white" onClick={() => setCurrentMenu('VOLTAGE')}>Voltage</button>
              <button className="text-left pl-2 text-white" onClick={() => setCurrentMenu('POWERFACTOR')}>PowerFactor</button>
              <button className="text-left pl-2 text-white" onClick={() => setCurrentMenu('ACTIVEPOWER')}>ActivePower</button>
              <button className="text-left pl-2 text-white" onClick={() => setCurrentMenu('FREQUENCY')}>Frequency</button>
            </div>
          </div>
          <div className="flex flex-row space-x-2 hover:bg-sky-700 pl-1" onClick={() => setLocalRoute('TABLE')}>
            <Table />
            <p className="text-white text-xl">Table</p>
          </div>
          <div className="flex flex-row space-x-2 hover:bg-sky-700 pl-1" onClick={() => setLocalRoute('REPORT')}>
            <Report />
            <p className="text-white text-xl">Report</p>
          </div>
        </div>
        <div className="w-full h-full" >
          {routeElement}
        </div>
      </div>
    </div >
  )
}

export default Dashboard