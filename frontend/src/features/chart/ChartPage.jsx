import { useEffect, useState } from 'react';
import { Line, Chart } from 'react-chartjs-2'
import { Chart as ChartJS, registerables } from 'chart.js';
// Current 0A-400A
// Voltage 0V-500V
// Power Factor -1-1
// Active Power 0kW-250kW
// Frequency 0hz-50hz
ChartJS.register(...registerables);

export function CurrentChart(props) {
  const { data } = props;

  return (
    <Chart type='line'
      options={{
        maintainAspectRatio: false,
        animation: false,
        scales: {
          y: {
            display: true,
            min: 0,
            max: 400,
          },
        },
      }}
      data={{
        labels: data.map((value) => value.label),
        datasets: [{
          label: 'Current (A)',
          data: data.map((value) => value.current),
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0,
        }],
      }} />
  )
}

export function VoltageChart(props) {
  const { data } = props;

  return (
    <Chart type='line'
      options={{
        maintainAspectRatio: false,
        animation: false,
        scales: {
          y: {
            display: true,
            min: 0,
            max: 500,
          },
        },
      }}
      data={{
        labels: data.map((value) => value.label),
        datasets: [{
          label: 'Voltage (V)',
          data: data.map((value) => value.voltage),
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0,
        }],
      }} />
  )
}

export function PowerFactorChart(props) {
  const { data } = props;

  return (
    <Chart type='line'
      options={{
        maintainAspectRatio: false,
        animation: false,
        scales: {
          y: {
            display: true,
            min: -1,
            max: 1,
          },
        },
      }}
      data={{
        labels: data.map((value) => value.label),
        datasets: [{
          label: 'Power Factor',
          data: data.map((value) => value.powerFactor),
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0,
        }],
      }} />
  )
}

export function ActivePowerChart(props) {
  const { data } = props;

  return (
    <Chart type='line'
      options={{
        maintainAspectRatio: false,
        animation: false,
        scales: {
          y: {
            display: true,
            min: 0,
            max: 250,
          },
        },
      }}
      data={{
        labels: data.map((value) => value.label),
        datasets: [{
          label: 'Active Power (kW)',
          data: data.map((value) => value.activePower),
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0,
        }],
      }} />
  )
}

export function FrequencyChart(props) {
  const { data } = props;
  return (
    <Chart type='line'
      options={{
        maintainAspectRatio: false,
        animation: false,
        scales: {
          y: {
            display: true,
            min: 0,
            max: 50,
          },
        },
      }}
      data={{
        labels: data.map((value) => value.label),
        datasets: [{
          label: 'Frequency (Hz)',
          data: data.map((value) => value.frequency),
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0,
        }],
      }} />
  )
}

export default function ChartPage({ dataset }) {
  return (
    <div className="h-full w-full bg-white flex-col">
      <div className="h-1/2 w-full flex flex-row">
        <div className="h-full w-1/2 borderize">
          <CurrentChart data={dataset} />
        </div>
        <div className="h-full w-1/2 borderize" >
          <VoltageChart data={dataset} />
        </div>
      </div>
      <div className="h-1/2 w-full flex flex-row">
        <div className="h-full w-1/3 borderize" >
          <PowerFactorChart data={dataset} />
        </div>
        <div className="h-full w-1/3 borderize" >
          <ActivePowerChart data={dataset} />
        </div>
        <div className="h-full w-1/3 borderize" >
          <FrequencyChart data={dataset} />
        </div>
      </div>
    </div>
  )
}