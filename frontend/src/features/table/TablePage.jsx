function TableRow(props) {
  const {date, current, voltage, powerFactor, activePower, frequency} = props;

  return (
    <tr>
      <th className="border-2 border-black">{date}</th>
      <th className="border-2 border-black">{current}</th>
      <th className="border-2 border-black">{voltage}</th>
      <th className="border-2 border-black">{powerFactor}</th>
      <th className="border-2 border-black">{activePower}</th>
      <th className="border-2 border-black">{frequency}</th>
    </tr>
  ) 
}

export default function TablePage({ dataset }) {
  const dummyData = [
    {date: 'test date 1', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 2', current: 120, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 3', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 4', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 5', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 6', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
    {date: 'test date 7', current: 150, voltage: 80, powerFactor: 1, activePower: 200, frequency: 50},
  ];

  const tableRows = dataset.map((data) => {
    return <TableRow id={`${data.date}${data.current}`} date={data.date} current={data.current} voltage={data.voltage} powerFactor={data.powerFactor} activePower={data.activePower} frequency={data.frequency} />
  });

  return <div>
    <table className="table-auto border-collapse border-2 border-black w-full">
  <thead className="bg-sky-100">
    <tr>
      <th className="border-2 border-black">Tanggal</th>
      <th className="border-2 border-black">Ampere</th>
      <th className="border-2 border-black">Voltage</th>
      <th className="border-2 border-black">PowerFactor</th>
      <th className="border-2 border-black">kW</th>
      <th className="border-2 border-black">Frequency</th>
    </tr>
  </thead>
  <tbody>
    {tableRows}
  </tbody>
</table>
  </div>
}