<?php

namespace App\Http\Controllers;

use App\Models\Listrik;
use Illuminate\Http\Request;

class ListrikController extends Controller
{
  function index()
  {
    return response(Listrik::all());
  }

  function table(Request $req)
  {
    $listrik = new Listrik();
    $listrik->date = $req->input('date');
    $listrik->current = $req->input('current');
    $listrik->voltage = $req->input('voltage');
    $listrik->powerFactor = $req->input('powerFactor');
    $listrik->activePower = $req->input('activePower');
    $listrik->frequency = $req->input('frequency');

    $listrik->save();
    return response($listrik, 201);
  }
}
